use nannou::prelude::*;
use serde_derive::Deserialize;
use std::env;
use std::fs;
use toml::de;

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

struct Model {
    color: Hsl, // color of the ellipse
    // needed for the workflow
    root: String,  // root folder of the model
    max_time: u32, // for how long the experiment runs
    counter: u32,  // counter variable to know when the experiment is done.
}

impl Model {
    fn update_counter(&mut self) {
        self.counter += 1;
    }
}

#[derive(Deserialize)]
struct Input {
    max_time: u32,
    color: (f32, f32, f32),
}

fn model_from_file(_app: &App, root: String) -> Model {
    // get the model description from the input file.
    let contents = fs::read_to_string(root.clone() + "/input.toml").expect("Computer says no.");
    let content_type: Input = de::from_str(&contents).unwrap();
    // create and return model.
    Model {
        root: root,
        color: hsl(
            content_type.color.0,
            content_type.color.1,
            content_type.color.2,
        ),
        max_time: content_type.max_time,
        counter: 0,
    }
}

fn model(_app: &App) -> Model {
    // read root folder from args.
    let args: Vec<String> = env::args().collect();
    let default_root: std::string::String = "default".to_string();
    let root: String;
    if args.len() < 2 {
        root = default_root;
    } else {
        root = args[1].clone();
    }
    // create model from file.
    model_from_file(_app, root)
}

fn update(app: &App, model: &mut Model, _update: Update) {
    model.update_counter();
    if model.counter > model.max_time {
        app.quit();
    } // else {
      //     println!("counter stands at {}", model.counter);
      // }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    draw.background().color(BLACK);

    draw.ellipse().color(model.color);
    draw.to_frame(app, &frame).unwrap();

    // to create the png files. takes forever to actually do.
    let sub_path = format!("/capture/frame-{:04}.png", frame.nth());
    let file_path = model.root.clone() + &sub_path.to_string();
    app.main_window().capture_frame(file_path);

    //fmpeg command
    // ffmpeg -framerate 60 -i capture/frame-%04d.png -pix_fmt yuv420p -vcodec libx264 -preset veryslow -tune animation -crf 18 -r 60 out.mp4
}
