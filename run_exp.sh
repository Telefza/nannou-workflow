#!/bin/bash

folders=(setting*)
# echo ${folders[1]}
# loop over all setting folders
for root in "${folders[@]}"; do
    # cleanup by deleting whatever was catpured and created in a previous run
    if [ -d "${root}/capture" ]; then
	echo "deleting ${root}/capture"
	rm "${root}/capture" -r
    fi
    if [ -f  "${root}/${root}_out.mp4" ]; then
	echo "deleting ${root}/${root}_out.mp4"
	rm "${root}/${root}_out.mp4"
    fi
    # run the nannou app given the root folder
    cargo run ${root}
    # convert captured output to mp4 file.
    ffmpeg -framerate 60 -i "${root}/capture/frame-%04d.png" -pix_fmt yuv420p -vcodec libx264 -preset veryslow -tune animation -crf 18 -r 60 "${root}/${root}_out.mp4"
done
